# Python 2019-08

Python kurss Rīgas Programmēšanas skolā.

### 1. nodarbība, 2019-08-28

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master)

Noderīgas saites:

* https://www.anaconda.com/distribution
* https://docs.anaconda.com/anaconda/install/
* https://www.sublimetext.com/3


### 2. nodarbība, 2019-08-30

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=00-objekti%20un%20datu%20strukturas%2Fempty.ipynb)

### 3. nodarbība, 2019-08-31

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=00-objekti%20un%20datu%20strukturas%2Fempty.ipynb)

### Datu struktūras

Pārbaudes darbs
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=00-objekti%20un%20datu%20strukturas%2FObjects%20and%20Data%20Structures%20Assessment%20Test.ipynb)

### 4. nodarbība, 2019-09-04

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=00-objekti%20un%20datu%20strukturas%2Fempty.ipynb)


### 5. nodarbība, 2019-09-06

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=00-objekti%20un%20datu%20strukturas%2Fempty.ipynb)

Pārbaudes darbs
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=02-deklaracijas%2F07-Statements%20Assessment%20Test.ipynb)

Bonus challenge
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=02-deklaracijas%2FGuessing_Game_Challenge.ipynb)


### 6. nodarbība, 2019-09-11

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=00-objekti%20un%20datu%20strukturas%2Fempty.ipynb)

### 7. nodarbība, 2019-09-13 (piektdiena)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=00-objekti%20un%20datu%20strukturas%2Fempty.ipynb)

Uzdevumi
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=03-metodes%20un%20funkcijas%2F03-Function%20Practice%20Exercises.ipynb)

Mājasdarbs
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=03-metodes%20un%20funkcijas%2F08-Functions%20and%20Methods%20Homework.ipynb)

### 8. nodarbība, 2019-09-14 (sestdiena)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=00-objekti%20un%20datu%20strukturas%2Fempty.ipynb)

Bonus mājasdarbs [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=05-Object%20Oriented%20Programming%2F02-Object%20Oriented%20Programming%20Homework.ipynb)

###### 9. nodarbība, 2019-09-18 (trešdiena)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=00-objekti%20un%20datu%20strukturas%2Fempty.ipynb)

###### 10. nodarbība, 2019-09-20 (piektdiena)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=00-objekti%20un%20datu%20strukturas%2Fempty.ipynb)

###### 11. nodarbība, 2019-09-25 (trešdiena)

GUI paraugs [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=gui.ipynb)

Datu Analīze [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mrtmednis%2Fpython-2019-08/master?filepath=file_processing_empty.ipynb)

###### 12. nodarbība, 2019-09-27 (piektdiena)

###### 13. nodarbība, 2019-09-28 (sestdiena)

Datetime formatting, Decorators, timeit, Advanced OOP

###### 14. nodarbība, 2019-10-02 (trešdiena)

Regex,

###### 15. nodarbība, 2019-10-04 (piektdiena), noslēgums
